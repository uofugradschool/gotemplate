package gotemplate

import (
	"strings"
	"testing"
)

func TestRead(t *testing.T) {

	template := Template{BaseURL: "."}
	index, err := template.Read("/index.html")
	if err != nil {
		t.Error(err)
	}
	if strings.TrimSpace(index) != "<!DOCTYPE html>" {
		t.Error(
			"expected", "<!DOCTYPE html>",
			"got", index)
	}
}

func TestGetTeamplateNamesWithSlash(t *testing.T) {

	template := Template{BaseURL: "."}
	s := "<& templates layout.html.mustache partials/sidebar.html.mustache form_a.html.mustache form-b.html.mustache &>"

	fileNames := template.GetTemplateNames(s)

	trigger := false
	for _, val := range fileNames {
		if val == "partials/sidebar.html.mustache" {
			trigger = true
		}
	}

	if !trigger {
		t.Error("Expected partials/sidebar.html.mustache", "template name not present", fileNames)
	}

}

func TestGetTemplateNames(t *testing.T) {

	template := Template{BaseURL: "."}
	s := "<& templates layout.html.mustache sidebar.html.mustache form_a.html.mustache form-b.html.mustache &>"

	fileNames := template.GetTemplateNames(s)

	for _, val := range fileNames {
		if val == "template" || val == "templates" {
			t.Error("template is not a file name", fileNames)
		}
	}
	var check bool = false
	for _, val := range fileNames {
		if val == "layout.html.mustache" {
			check = true
		}
	}
	if !check {
		t.Error("layout.html.mustahce is missing from file names", fileNames)
	}
	//reset
	check = false
	for _, val := range fileNames {
		if val == "sidebar.html.mustache" {
			check = true
		}
	}
	if !check {
		t.Error("sidebar.html.mustahce is missing from file names", fileNames)
	}
	//reset
	check = false
	for _, val := range fileNames {
		if val == "form_a.html.mustache" {
			check = true
		}
	}
	if !check {
		t.Error("form_a.html.mustahce is missing from file names", fileNames)
	}
	//reset
	check = false
	for _, val := range fileNames {
		if val == "form-b.html.mustache" {
			check = true
		}
	}
	if !check {
		t.Error("form-b.html.mustahce is missing from file names", fileNames)
	}

	multiLineString := "This String is Mulit\nLine. but this should not create an issue\n when checking \n <& template layout.html.mustache &> for the template string."

	multiLine := template.GetTemplateNames(multiLineString)

	//reset
	check = false
	for _, val := range multiLine {
		if val == "layout.html.mustache" {
			check = true
		}
	}

	if !check {
		t.Error("Missing file layout.html.mustache", multiLine)
	}

	//reset
	check = false
	if len(multiLine) != 1 {
		t.Error("MultiLine test", "expected", "multiLine length == 1", "got", len(multiLine), multiLine)
	}

}

func TestRemoveDuplicateFileNames(t *testing.T) {

	template := Template{BaseURL: "."}

	duplicates := [][]string{[]string{"one", "one", "two"}, []string{"one", "two", "three"}, []string{"three", "four", "one", "two", "four"}}

	result := template.RemoveDuplicateFileNames(duplicates)

	if len(result) != 4 {
		t.Error("Expected", "4 file names", "Got", len(result))
	}

	var check bool = false

	for _, val := range result {
		if val == "one" {
			check = true
		}
	}

	if !check {
		t.Error("missing file 'one'")
	}

	//reset
	check = false
	for _, val := range result {
		if val == "two" {
			check = true
		}
	}

	if !check {
		t.Error("missing file 'two'")
	}

	//reset
	check = false
	for _, val := range result {
		if val == "three" {
			check = true
		}
	}

	if !check {
		t.Error("missing file 'three'")
	}

	//reset
	check = false
	for _, val := range result {
		if val == "four" {
			check = true
		}
	}

	if !check {
		t.Error("missing file 'four'")
	}

	for _, val := range result {
		if val != "one" && val != "two" && val != "three" && val != "four" {
			t.Error("File name not recognized", val)
		}
	}
}

func TestGetTemplate(t *testing.T) {
	gt := Template{BaseURL: "./testViews/"}

	_, err := gt.GetTemplate("main.html.mustache")
	if err != nil {
		t.Error(err)
	}
	//	t.Error("expected string", "got", template)
}
