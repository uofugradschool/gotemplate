package gotemplate

import (
	"io/ioutil"
	//"errors"
	"regexp"
	"strings"
)

type Template struct {
	BaseURL string
}

func (t *Template) GetTemplate(file string) (string, error) {

	fileContent := make(map[string]string)
	fileTemplates := make(map[string][]string)

	template, err := t.Read(file)
	if err != nil {
		return "", err
	}

	/// seed maps
	// add file to map
	fileContent[file] = template
	fileTemplates[file] = t.GetTemplateNames(template)

	loop := true
	for loop {
		loop = false

		// loop through templates list of required templates
		for _, val := range fileTemplates {

			// loop through required templates
			for _, templateName := range val {

				// check if template alread loaded
				if _, ok := fileTemplates[templateName]; !ok {

					//not loaded already loaded
					// flag to loop again once loaded
					loop = true

					// load template contents into FileContent
					content, err := t.Read(templateName)
					if err != nil {
						return "", err
					}

					fileContent[templateName] = content

					// Check for new required Templates
					fileTemplates[templateName] = t.GetTemplateNames(content)

				}
			}
		}
	}

	return t.Combine(fileContent), nil
}

//Read file reads a file and returns the contents in a string
func (t *Template) Read(file string) (string, error) {

	contents, err := ioutil.ReadFile(t.BaseURL + file)
	if err != nil {
		return "", err
	}
	return string(contents), nil

}

//Get Template Names creates one string containing the contents of all templates
//listed in the the templates tags in the the template files.
// will return an empty slice if no templates found
func (t *Template) GetTemplateNames(content string) []string {

	templateLineRegex := regexp.MustCompile("(?im)<& *template.*&>")
	templatesRegex := regexp.MustCompile(`([\w|\.|\-|\/]+)`)
	templateThrowAwayRegex := regexp.MustCompile("(?i)templates*$")
	//match words only (no white spaceor empty strings)
	wordOnlyRegex := regexp.MustCompile("^\\w+")

	fileNames := make([]string, 0)

	templateLine := templateLineRegex.FindString(content)

	if len(templateLine) > 0 {
		templates := templatesRegex.FindAllStringSubmatch(templateLine, -1)

		for _, val := range templates {
			//if match is "template" continue
			if templateThrowAwayRegex.MatchString(val[1]) {
				continue
			}
			//match words only (no white spaceor empty strings)
			if wordOnlyRegex.MatchString(val[1]) {
				fileNames = append(fileNames, val[1])
			}
		}

	}

	return fileNames
}

// NOT USED FOR NOW. THIS IS HANDLED ANOTHER WAY
//Remove Duplicate file names and return a single []string with file names
func (t *Template) RemoveDuplicateFileNames(s [][]string) []string {

	finalList := make([]string, 0)
	fileNameMap := make(map[string]string)

	for _, outer := range s {
		for _, val := range outer {
			if _, present := fileNameMap[val]; !present {
				fileNameMap[val] = val
			}
		}
	}

	for _, val := range fileNameMap {
		finalList = append(finalList, val)
	}
	return finalList
}

func (t *Template) LoadAllTemplates(fileNames []string) map[string]string {

	result := make(map[string]string)

	for _, val := range fileNames {
		file, err := t.Read(val)
		if err != nil {
			continue
		}

		result[val] = file
	}

	return result
}

func (t *Template) Combine(content map[string]string) string {
	/*
		fmt.Println("====COMBINE======")
		fmt.Println(content)
		fmt.Println("====COMBINE======")
	*/
	tempString := make([]string, 0)
	var finalString string

	//REGEX
	templateTag := regexp.MustCompile("(?ims)<&\\s+templates*\\s+.*?&>\\s*?[^\\w|<|>|-]")

	gets := regexp.MustCompile("(?ism)<&\\s+([\\w|-]+)\\s+&>")

	putMatch := regexp.MustCompile("(?ims)<&\\s+put\\s")
	putOpenTag := regexp.MustCompile("(?ims)<&\\s+put\\s+([\\w|-]+)\\s+?&>")
	putCloseTag := regexp.MustCompile("(?ims)<&\\s+end\\s+([\\w|-]+)\\s+?&>")

	notBlank := regexp.MustCompile("(?ism)[^\\s]")

	//maps
	putMap := make(map[string]string)

	// for each content
	for key, val := range content {
		//fmt.Println(":::", key, val)
		//clear template line
		val = templateTag.ReplaceAllString(val, "")

		//check if has a put statement
		hasPut := putMatch.MatchString(val)
		//fmt.Println(hasPut)
		if hasPut {
			//get index values of put tags
			putOpenTagIndex := putOpenTag.FindStringIndex(val)
			//fmt.Println(putOpenTagIndex)
			//get put tag -- start search from index
			putTag := putOpenTag.FindStringSubmatch(val[putOpenTagIndex[0]:])
			putCloseTagIndex := putCloseTag.FindStringIndex(val[putOpenTagIndex[0]:])
			// grab between the tags
			putMap[putTag[1]] = val[putOpenTagIndex[1]+1 : putOpenTagIndex[0]+putCloseTagIndex[0]-1]

			val = val[:putOpenTagIndex[0]] + val[putOpenTagIndex[0]+putCloseTagIndex[1]:]
		}
		// put outside has put to make sure to catch everything
		content[key] = val
	}

	//fmt.Println(content)
	for _, val := range content {

		include := notBlank.MatchString(val)
		if include {
			tempString = append(tempString, val)
		}
	}

	finalString = strings.Join(tempString, "\n")
	// loop  until all gets are cleared
	// this is because adding puts could add more gets
	// for nested gets
	for gets.MatchString(finalString) {
		finalString = gets.ReplaceAllStringFunc(finalString, func(s string) string {
			// get just tag
			tag := gets.FindAllStringSubmatch(s, 1)

			if putString, ok := putMap[tag[0][1]]; ok {
				return putString
			} else {
				return ""
			}
		})
	}
	//fmt.Println(finalString)
	return finalString

}
